# Using official InfluxData 1.x Sandbox


This repo is a quick way to get the entire 1.x TICK Stack spun up and working together. It uses [Docker](https://www.docker.com/) to spin up the full TICK stack in a connected 
fashion.

## Getting Started

        git clone git@gitlab.com:miroslav.krnoul/humor-zkusebka.git

        cd ./humor-zkusebka/monitoring-stack

        ./sandbox up

### Running

To run the `sandbox`, simply use the convenient cli:

```bash
$ ./sandbox
sandbox commands:
  up           -> spin up the sandbox environment (add -nightly to grab the latest nightly builds of InfluxDB and Chronograf)
  down         -> tear down the sandbox environment
  restart      -> restart the sandbox
  influxdb     -> attach to the influx cli
  flux         -> attach to the flux REPL

  enter (influxdb||kapacitor||chronograf||telegraf) -> enter the specified container
  logs  (influxdb||kapacitor||chronograf||telegraf) -> stream logs for the specified container

  delete-data  -> delete all data created by the TICK Stack
  docker-clean -> stop and remove all running docker containers
  rebuild-docs -> rebuild the documentation container to see updates
```

To get started just run `./sandbox up`. You browser will open documentation:

- `localhost:3010` - Documentation server. This contains a simple markdown server for tutorials and documentation.
- `localhost:3000` - Grafana  `admin/admin`   dashboards uses Influx and Prometheus datasources


