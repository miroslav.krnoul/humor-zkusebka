docker exec mysql-repl-cluster-mysql-master01-1 bash telegraf-init.sh
docker exec mysql-repl-cluster-mysql-master02-1 bash telegraf-init.sh
docker exec mysql-repl-cluster-mysql-slave01-1 bash telegraf-init.sh
docker exec mysql-repl-cluster-mysql-slave02-1 bash telegraf-init.sh
docker exec mysql-repl-cluster-mysql-slave03-1 bash telegraf-init.sh
docker exec mysql-repl-cluster-mysql-slave04-1 bash telegraf-init.sh
