# MySQL cluster + Prometheus using Docker



## Configuration



- MySQL multi-source replication
  - `master01, master02`
  - `slave01, slave02, slave03, slave04`
  - `user`: root, `pass`: password

- Prometheus
  - `:9090`
  - using `msqld-exporter` to obtain mysql metrics

- Grafana
  - `:3000`
  - `id`: admin
  - `password`: admin

- ProxySQL (very basic implementation)
  - `:6032`  (admin, user: admin2, pass: pass2)
  - `:6033` (MySQL endpoint, user: root, pass: password)

## Getting Started
clone when run separately only

        git clone git@gitlab.com:miroslav.krnoul/humor-zkusebka.git 

        cd ./humor-zkusebka/mysql-repl-cluster

        $ ./mysql.sh
        
    Wait about two minutes for complete cluster initialization

## Check status

- MySQL master

    ```
    $ docker compose exec mysql-master01 sh -c "export MYSQL_PWD=password; mysql -u root -e 'SHOW REPLICAS\G'"
    $ docker compose exec mysql-master02 sh -c "export MYSQL_PWD=password; mysql -u root -e 'SHOW REPLICAS\G'"
    ```
     you should see three slaves `master02, slave01, slave02` behind master01, and two slaves `slave03, slave04` behind master02

- MySQL slave

    ```
    $ docker compose exec mysql-slave01 sh -c "export MYSQL_PWD=password; mysql -u root -e 'SHOW SLAVE STATUS\G'"
    ```
    if you needed:

    ```
    $ docker compose exec mysql-slave02 sh -c "export MYSQL_PWD=password; mysql -u root -e 'SHOW SLAVE STATUS\G'"
    $ docker compose exec mysql-slave03 sh -c "export MYSQL_PWD=password; mysql -u root -e 'SHOW SLAVE STATUS\G'"
    $ docker compose exec mysql-slave04 sh -c "export MYSQL_PWD=password; mysql -u root -e 'SHOW SLAVE STATUS\G'"
    ```


## View DB metrics on Grafana

1. Access `localhost:3000` and login. (`id`: admin, `pass`: admin)

2. Go `Dashboards  > MySQL Replication or MySQL Overview`



## Run benchmarks (sysbench)
  
        $ sudo apt-get install sysbench

1. write benchmark:

```
$ sysbench --db-driver=mysql \
        --mysql-host=0.0.0.0 \
        --mysql-port=6033 \
        --mysql-user=root \
        --mysql-password=password \
        --mysql-db=sbtest \
        --threads=10 \
        --tables=100 \
        --table-size=10000 \
        oltp_read_only \
        prepare
```
    you can watch replication delay on Grafana `MySQL Replication` dashboard 

2. read benchmark:

```
$ sysbench --db-driver=mysql \
        --mysql-host=0.0.0.0 \
        --mysql-port=6033 \
        --mysql-user=root \
        --mysql-password=password \
        --mysql-db=sbtest \
        --threads=100 \
        --time=120 \
        oltp_read_only \
        run
```



