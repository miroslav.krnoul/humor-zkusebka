
# MongoDB cluster by recommended production schema 

https://www.mongodb.com/docs/manual/core/sharded-cluster-components/

MongoDB Cluster on Docker with **PSA Style** (Primary - Secondary - Arbiter)

        https://www.mongodb.com/docs/v4.2/_images/replica-set-primary-with-secondary-and-arbiter.bakedsvg.svg

![My Remote Image](https://www.mongodb.com/docs/v4.2/_images/replica-set-primary-with-secondary-and-arbiter.bakedsvg.svg?dl=0)

## Configuration

- 2 Mongos (router):
  - router01
  - router02
- 3 Config Servers:
  - configsvr01
  - configsvr02
  - configsvr03
- 3 Shards (each a 2 members of replica set and one arbiter): 
  - shard01-a, shard01-b, shard01-arb
  - shard02-a, shard02-b, shard02-arb
  - shard03-a, shard03-b, shard03-arb


## Getting Started

        git clone git@gitlab.com:miroslav.krnoul/humor-zkusebka.git

        cd ./humor-zkusebka/mongodb-shard-cluster

        docker compose up -d

        [+] Running 15/15



## Config-servers, shards and routers initialization:

### Config servers

        $ ./shell/configservers.sh 

      you should see one "ok"


### Shards


#### Initializing the shards

        $ ./shell/shards.sh
 
    you should see 3x "ok"

#### Add Arbiters

Make sure shards "A" are primary

        $ ./shell/status.sh

			"name" : "shard01-a:27017",
			"stateStr" : "PRIMARY",
			"name" : "shard01-b:27017",
			"stateStr" : "SECONDARY",
			"name" : "shard02-a:27017",
			"stateStr" : "PRIMARY",
			"name" : "shard02-b:27017",
			"stateStr" : "SECONDARY",
			"name" : "shard03-a:27017",
			"stateStr" : "PRIMARY",
			"name" : "shard03-b:27017",
			"stateStr" : "SECONDARY",

then add arbiters

        $ ./shell/addarb.sh

     3x "ok"


### Routers

After initilizing `config-server` and `shards`, wait a bit to to elect primaries
before initilizing `router`:

run './shell/status.sh' again

        $ ./shell/status.sh 

			"name" : "shard01-a:27017",
			"stateStr" : "PRIMARY",
			"name" : "shard01-b:27017",
			"stateStr" : "SECONDARY",
			"name" : "shard01-arb:27017",
			"stateStr" : "ARBITER",
			"name" : "shard02-a:27017",
			"stateStr" : "PRIMARY",
			"name" : "shard02-b:27017",
			"stateStr" : "SECONDARY",
			"name" : "shard02-arb:27017",
			"stateStr" : "ARBITER",
			"name" : "shard03-a:27017",
			"stateStr" : "PRIMARY",
			"name" : "shard03-b:27017",
			"stateStr" : "SECONDARY",
			"name" : "shard03-arb:27017",
			"stateStr" : "ARBITER",


when OK, initialize routers


        $ ./shell/routers.sh 

    3x "ok"



## Verify status

### Check the status of shards:


        $ docker exec -it shard-01-node-a bash -c "echo 'rs.status(\""\"")' | mongo --port 27017"

        docker exec -it shard-02-node-a bash -c "echo 'rs.status(\""\"")' | mongo --port 27017"
        docker exec -it shard-03-node-a bash -c "echo 'rs.status(\""\"")' | mongo --port 27017"

### Check the status of router:

        $ docker exec -it router-01 bash -c "echo 'sh.status(\""\"")' | mongo --port 27017"

        MongoDB shell version v4.2.22
        connecting to: mongodb://127.0.0.1:27017/?compressors=disabled&gssapiServiceName=mongodb
        Implicit session: session { "id" : UUID("98f061f2-a16f-4673-941d-2b6f3b1166ab") }
        MongoDB server version: 4.2.22
        --- Sharding Status --- 
          sharding version: {
          	"_id" : 1,
          	"minCompatibleVersion" : 5,
          	"currentVersion" : 6,
          	"clusterId" : ObjectId("63109e24e85cbcb6b263e69e")
          }
          shards:
                {  "_id" : "rs-shard-01",  "host" : "rs-shard-01/shard01-a:27017,shard01-b:27017",  "state" : 1 }
                {  "_id" : "rs-shard-02",  "host" : "rs-shard-02/shard02-a:27017,shard02-b:27017",  "state" : 1 }
                {  "_id" : "rs-shard-03",  "host" : "rs-shard-03/shard03-a:27017,shard03-b:27017",  "state" : 1 }
          active mongoses:
                "4.2.22" : 2
          autosplit:
                Currently enabled: yes
          balancer:
                Currently enabled:  yes
                Currently running:  yes
                Collections with active migrations: 
                        config.system.sessions started at Thu Sep 01 2022 12:35:00 GMT+0000 (UTC)
                Failed balancer rounds in last 5 attempts:  0
                Migration Results for the last 24 hours: 
                        41 : Success
          databases:
                {  "_id" : "config",  "primary" : "config",  "partitioned" : true }
                        config.system.sessions
                                shard key: { "_id" : 1 }
                                unique: false
                                balancing: true
                                chunks:
                                        rs-shard-01	982
                                        rs-shard-02	21
                                        rs-shard-03	21
                                too many chunks to print, use verbose if you want to force print

        bye

repeat this, you can watch chunks balancing


## Sample data

        $ docker cp restaurants.json router-01:/
        $ docker exec -it router-01 bash 
        root@dbf68c65168c:/# mongo

        mongos> sh.enableSharding("places")

    1x "ok"

        mongos> sh.shardCollection("places.restaurants", { restaurant_id: 1 } )

    1x "ok"

        mongos> exit
        bye

        root@dbf68c65168c:/# mongoimport --db=places --collection=restaurants --file=restaurants.json --jsonArray
        2022-09-01T12:47:19.562+0000	connected to: mongodb://localhost/
        2022-09-01T12:47:19.780+0000	3772 document(s) imported successfully. 0 document(s) failed to import.
        root@dbf68c65168c:/# exit

check router:

         $ docker exec -it router-01 bash -c "echo 'sh.status(\""\"")' | mongo --port 27017"        
        MongoDB shell version v4.2.22
        connecting to: mongodb://127.0.0.1:27017/?compressors=disabled&gssapiServiceName=mongodb
        Implicit session: session { "id" : UUID("bf06b504-ba31-438a-a18c-fdcea410287c") }
        MongoDB server version: 4.2.22
        --- Sharding Status --- 
          sharding version: {
          	"_id" : 1,
          	"minCompatibleVersion" : 5,
          	"currentVersion" : 6,
          	"clusterId" : ObjectId("6322e7151609665073c30eea")
          }
          shards:
                {  "_id" : "rs-shard-01",  "host" : "rs-shard-01/shard01-a:27017,shard01-b:27017",  "state" : 1 }
                {  "_id" : "rs-shard-02",  "host" : "rs-shard-02/shard02-a:27017,shard02-b:27017",  "state" : 1 }
                {  "_id" : "rs-shard-03",  "host" : "rs-shard-03/shard03-a:27017,shard03-b:27017",  "state" : 1 }
          active mongoses:
                "4.2.22" : 2
          autosplit:
                Currently enabled: yes
          balancer:
                Currently enabled:  yes
                Currently running:  no
                Failed balancer rounds in last 5 attempts:  0
                Migration Results for the last 24 hours: 
                        524 : Success
          databases:
                {  "_id" : "config",  "primary" : "config",  "partitioned" : true }
                        config.system.sessions
                                shard key: { "_id" : 1 }
                                unique: false
                                balancing: true
                                chunks:
                                        rs-shard-01	500
                                        rs-shard-02	262
                                        rs-shard-03	262
                                too many chunks to print, use verbose if you want to force print
                {  "_id" : "places",  "primary" : "rs-shard-03",  "partitioned" : true,  "version" : {  "uuid" : UUID("5fc2db6f-6739-4003-a3a1-effea06eda9f"),  "lastMod" : 1 } }
                        places.restaurants
                                shard key: { "restaurant_id" : 1 }
                                unique: false
                                balancing: true
                                chunks:
                                        rs-shard-03	1
                                { "restaurant_id" : { "$minKey" : 1 } } -->> { "restaurant_id" : { "$maxKey" : 1 } } on : rs-shard-03 Timestamp(1, 0) 

                










