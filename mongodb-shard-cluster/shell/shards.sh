docker compose exec shard01-a sh -c "mongo < /scripts/replicaset_1.js"
docker compose exec shard02-a sh -c "mongo < /scripts/replicaset_2.js"
docker compose exec shard03-a sh -c "mongo < /scripts/replicaset_3.js"

primary=0

until [ $primary = 3 ]
do
  echo "wait for initialize shards..."
  primary=`./shell/status.sh | grep -c PRIMARY`
  sleep 2
done
