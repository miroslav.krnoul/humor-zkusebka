#!/bin/bash

docker exec -it shard-01-node-a bash -c "echo 'rs.addArb(\""shard01-arb:27017\"")' | mongo --port 27017"

docker exec -it shard-02-node-a bash -c "echo 'rs.addArb(\""shard02-arb:27017\"")' | mongo --port 27017"

docker exec -it shard-03-node-a bash -c "echo 'rs.addArb(\""shard03-arb:27017\"")' | mongo --port 27017"



arbiter=0

until [ $arbiter = 3 ]
do
  echo "wait for initialize arbiters..."
  arbiter=`./shell/status.sh | grep -c ARBITER`
  sleep 2
done
