docker compose up -d

read -p "When OK, press Enter to init config servers"

./shell/configservers.sh 

read -p "When OK, press Enter to init shards"

./shell/shards.sh

./shell/status.sh

read -p "Press enter to add arbiters"

./shell/addarb.sh

./shell/status.sh

read -p "Press Enter to init routers"

./shell/routers.sh

./shell/sample.sh

echo "Cluster initialized OK"
