
# MongoDB Cluster on Docker with **PSA Style** (Primary - Secondary - Arbiter)

https://www.mongodb.com/docs/manual/core/sharded-cluster-components/


![My Remote Image](https://www.mongodb.com/docs/v4.2/_images/replica-set-primary-with-secondary-and-arbiter.bakedsvg.svg?dl=0)

## Configuration

- 2 Mongos (router):
  - router01
  - router02
- 3 Config Servers:
  - configsvr01
  - configsvr02
  - configsvr03
- 3 Shards (each a 2 members of replica set and one arbiter): 
  - shard01-a, shard01-b, shard01-arb
  - shard02-a, shard02-b, shard02-arb
  - shard03-a, shard03-b, shard03-arb



## Getting Started
clone when run separately only

        git clone git@gitlab.com:miroslav.krnoul/humor-zkusebka.git

        cd ./humor-zkusebka/mongodb-shard-cluster

        run ./makeall.sh

# DONE:-)

## Checking

        each server is accesible without password (no security requirements in the job specification)
        for example: 
            $ docker exec -it <container name> bash
            # mongo
            














