# Elasticsearch log stack (elasticsearch-cluster)

![Screenshot](elk.png)


## Configuration

- ELK
  - `metricbeat, slave01`
  - `kafka`
  - `logstash`
  - `elasticsearch multi-node` `es01, es02,es03`
  - `kibana`

        no passwords

## Getting Started
clone when run separately only
        $ git clone git@gitlab.com:miroslav.krnoul/humor-zkusebka.git

        $ cd ./humor-zkusebka/elasticsearch-cluster

        $ sudo sysctl -w vm.max_map_count=1048576

        $ docker compose up -d
        

## Check status (kafka)

        $ docker exec -it kafka kafka-console-consumer.sh --bootstrap-server=kafka:9092 BROKERS --topic metricbeat

    wait few minutes for cluster initialization 
    when running datastream appeared on console:

 `Ctrl` + `c` to exit

## Kibana

   open:
      [kibana](http://localhost:5601)

   go to `Stack management => Index management`

    here you can see the "metricbeat-%" indices created every minute (just for this exercise)
    all indices has one primary an one replica

    
