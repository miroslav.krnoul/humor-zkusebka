# Other technologies (other-prod-tech)

For a basic introduction to the technologies


## Configuration

- Other production technologies
  - `PostgreSql` 
  - `Adminer` [http://localhost:8080](http://localhost:8080) `system`:PostgreSQL, `server`:postgres, `user`:postgres, `password`:password
  - `ActiveMQ`[http://localhost:8161](http://localhost:8161) `user`:admin, `password`:admin
  - `Memcached`

Kafka is used on Elasticsearch log stack

## Getting Started
clone when run separately only

        $ git clone git@gitlab.com:miroslav.krnoul/humor-zkusebka.git

        $ cd ./humor-zkusebka/other-prod-tech

        $ docker compose up -d
        

