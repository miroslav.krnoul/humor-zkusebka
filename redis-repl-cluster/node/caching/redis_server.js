class redis_server {



    redisConnect() {


        const redis = require('redis');

        const redisClient = redis.createClient('127.0.0.1',26379);


        redisClient.connect();

        redisClient.on('error', err => {
            console.log('Error ' + err);
        });

        return redisClient;
    }

    setData(data) {
        var redisClient = this.redisConnect();
        redisClient.set('sbtest1', data);
        redisClient.expire('sbtest1', 10);
//        redisClient.set('sbtest1', data,'EX', 10);
    }

    getData(callBack) {
        var redisClient = this.redisConnect();
        var resp = redisClient.get('sbtest1');

        resp.then(function(result) {
            callBack(null, result)
        });
  
    }
               
}



module.exports = redis_server;

