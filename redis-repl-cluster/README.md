# Redis replica cluster (redis-repl-cluster)

## Configuration

- Redis cluster with Sentinel
  - `master01, slave01`
  - `sentinel01, sentinel02, sentinel03`
  - `metricbeat` for writing sample data(just timestamp every second)

## Getting Started
clone when run separately only

        git clone git@gitlab.com:miroslav.krnoul/humor-zkusebka.git

        cd ./humor-zkusebka/redis-repl-cluster

        $ docker compose up -d
        

## Check status

    `master:`
        docker exec -it sentinel01 redis-cli -h redis-master01 -p 6379 info| grep role

    `slave:`
        docker exec -it sentinel01 redis-cli -h redis-slave01 -p 6379 info| grep role

    sample data, reading from slave:
        docker exec -it redis-slave01 redis-cli -p 6379 lrange metricbeat 1 1000

## Test HA

    disable master:
        docker pause redis-master01

    redis-slave01 now has master role
        docker exec -it sentinel01 redis-cli -h redis-slave01 -p 6379 info| grep role
    
    and there's no gap in datastream:
        docker exec -it redis-slave01 redis-cli -p 6379 lrange metricbeat 1 1000
    
        docker unpause redis-master01
    master/slave roles are now switched

## Switching back

    disable actual master:
        docker pause redis-slave01
    
    check role:
        docker exec -it sentinel01 redis-cli -h redis-master01 -p 6379 info| grep role

        docker unpause redis-slave01

    Check status again:

    `master:`
        docker exec -it sentinel01 redis-cli -h redis-master01 -p 6379 info| grep role

    `slave:`
        docker exec -it sentinel01 redis-cli -h redis-slave01 -p 6379 info| grep role

    sample data:
        docker exec -it redis-slave01 redis-cli -p 6379 lrange metricbeat 1 1000

### still no gap in datastream:-)
